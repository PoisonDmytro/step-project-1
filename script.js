var decorativeTriangle = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    decorativeTriangle.setAttribute('xmlns', 'http://www.w3.org/2000/svg');
    decorativeTriangle.setAttribute('width', '25');
    decorativeTriangle.setAttribute('height', '13');
    decorativeTriangle.setAttribute('viewBox', '0 0 25 13');
    decorativeTriangle.setAttribute('fill', 'none');
    var pathElement = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    pathElement.setAttribute('fill-rule', 'evenodd');
    pathElement.setAttribute('clip-rule', 'evenodd');
    pathElement.setAttribute('d', 'M12.6977 12.4484L0.981557 0.720785H24.4136L12.6977 12.4484Z');
    pathElement.setAttribute('fill', '#18CFAB');
    decorativeTriangle.appendChild(pathElement);
    decorativeTriangle.classList.add("decorativeTriangle");

    document.querySelector('.service_active_btn').appendChild(decorativeTriangle);

serviceBtns.onclick = function(event){
    let oldBtn = document.querySelector('.service_active_btn');
    let oldcontent = document.querySelector(".service_active");
    let oldtriangle = document.querySelector('.decorativeTriangle')
    if(oldBtn != undefined){
        oldBtn.classList.remove("service_active_btn")
    }
    if(oldtriangle != undefined){
        oldtriangle.remove();
    }
    if(oldcontent != undefined){
        oldcontent.classList.remove("service_active")
    }

    let targetBtn = event.target;
    let target_btn = event.target.textContent.replace(/\s/g, '');
    let content = document.querySelector(`#${target_btn}`);
    targetBtn.classList.add('service_active_btn');
    targetBtn.appendChild(decorativeTriangle);
    content.classList.add("service_active")
}
//---------------
let oa_selectet_type = "all";
let loadMore = false;
let oa_imgList = [
    {
        url: "'/images/ImgList/17746-.jpg'",
        type:"gd"
    },
    {
        url: "'./images/ImgList/graficheskiy_dizayn3.png'",
        type:"gd"
    },
    {
        url: "'./images/ImgList/istockphoto-1165366649-612x612.jpg'",
        type:"gd"
    },
    {
        url: "'./images/ImgList/Quiz-for-designers.jpg'",
        type:"gd"
    },
    {
        url: "'./images/ImgList/web1jpg.jpg'",
        type:"wd"
    },
    {
        url: "'./images/ImgList/web2.jpg'",
        type:"wd"
    },
    {
        url: "'./images/ImgList/web3.jpeg'",
        type:"wd"
    },
    {
        url: "'./images/ImgList/web4.jpg'",
        type:"wd"
    },
    {
        url: "'./images/ImgList/lp1.png'",
        type:"lp"
    },
    {
        url: "'./images/ImgList/lp2.jpeg'",
        type:"lp"
    },
    {
        url: "'./images/ImgList/lp3.png'",
        type:"lp"
    },
    {
        url: "'./images/ImgList/lp4.jpg'",
        type:"lp"
    },
    {
        url: "'./images/ImgList/wp1.png'",
        type:"wp"
    },
    {
        url: "'./images/ImgList/wp2.png'",
        type:"wp"
    },
    {
        url: "'./images/ImgList/wp3.png'",
        type:"wp"
    },
    {
        url: "'./images/ImgList/wp4.jpg'",
        type:"wp"
    }
]
let oa_img_loadMore = [
    {
        url: "'/images/loadMore/graphic-design9.jpg'",
        type:"gd"
    },
    {
        url: "'/images/loadMore/graphic-design10.jpg'",
        type:"gd"
    },
    {
        url: "'/images/loadMore/graphic-design11.jpg'",
        type:"gd"
    },
    {
        url: "'/images/loadMore/graphic-design12.jpg'",
        type:"gd"
    },
    {
        url: "'/images/loadMore/landing-page4.jpg'",
        type:"lp"
    },
    {
        url: "'/images/loadMore/landing-page5.jpg'",
        type:"lp"
    },
    {
        url: "'/images/loadMore/landing-page6.jpg'",
        type:"lp"
    },
    {
        url: "'/images/loadMore/landing-page7.jpg'",
        type:"lp"
    },
    {
        url: "'/images/loadMore/web-design4.jpg'",
        type:"wd"
    },
    {
        url: "'/images/loadMore/web-design5.jpg'",
        type:"wd"
    },
    {
        url: "'/images/loadMore/web-design6.jpg'",
        type:"wd"
    },
    {
        url: "'/images/loadMore/web-design7.jpg'",
        type:"wd"
    },
    {
        url: "'/images/loadMore/wordpress7.jpg'",
        type:"wp"
    },
    {
        url: "'/images/loadMore/wordpress8.jpg'",
        type:"wp"
    },
    {
        url: "'/images/loadMore/wordpress9.jpg'",
        type:"wp"
    },
    {
        url: "'/images/loadMore/wordpress10.jpg'",
        type:"wp"
    },

]
for(item of oa_imgList){
    let card = document.createElement('div');
    card.style.backgroundImage = `url(${item.url})`;
    card.classList.add('oa_card');
    document.querySelector('.ourAmazing_imgList').append(card);
}
ourAmazing_btns.onclick = function(event){
    let activeBtnId = event.target.id;
    let oldActive = document.querySelector('.oa_active').classList.remove("oa_active");
    let newActive = event.target.classList.add("oa_active");
    
    oa_selectet_type = `${activeBtnId}`
    oa_generator()
}
oa_loadMore.onclick = function(event){
    if(loadMore == false){
        loadMore = true;
        oa_generator();
    }
}
function oa_generator(){
    let cards = document.querySelectorAll('.oa_card');
    for(card of cards){
        card.remove();
    }
    if(oa_selectet_type == "all"){
        for(item of oa_imgList){
            let card = document.createElement('div');
            card.style.backgroundImage = `url(${item.url})`;
            card.classList.add('oa_card')
            document.querySelector('.ourAmazing_imgList').append(card);
        }
        if(loadMore == true){
            for(item of oa_img_loadMore){
                let card = document.createElement('div');
                card.style.backgroundImage = `url(${item.url})`;
                card.classList.add('oa_card')
                document.querySelector('.ourAmazing_imgList').append(card);
            }
        }
    }else{
        for(item of oa_imgList){
            let card = document.createElement('div');
            if(item.type == oa_selectet_type){
                card.style.backgroundImage = `url(${item.url})`;
                card.classList.add('oa_card')
                document.querySelector('.ourAmazing_imgList').append(card);
            }
        }
        if(loadMore == true){
            for(item of oa_img_loadMore){
                let card = document.createElement('div');
                if(item.type == oa_selectet_type){
                    card.style.backgroundImage = `url(${item.url})`;
                    card.classList.add('oa_card')
                    document.querySelector('.ourAmazing_imgList').append(card);
                }
            }
        }
    }
}
//---------------
let cardBgelementsList = document.querySelectorAll('.card_bg_zone')
let count = 1;
for(card of cardBgelementsList){
    card.style.backgroundImage = `url("./images/breakingNews/${count}.png")`
    count++;
}
function newsHref(){
    window.location.href = "#"
}
//---------------
let peaple_info ={
    1:{
        name : "Arina ChikenKari",
        prof : "UX Designer"
    },
    2:{
        name : "Oleg MakBiceps",
        prof : "Personal Treiner"
    },
    3:{
        name : "Hasan Ali",
        prof : "UX Designer"
    },
    4:{
        name : "Katrima Forkarshtain",
        prof : "Necromancer"
    },
}
let pealeMenuPersonId = 3;
let peapleList = document.querySelectorAll('.peaple_menu_avatar');
peapleMenu.onclick = function(event){
    let targetBtnId = event.target.parentNode.parentNode.parentNode.id;
    if(targetBtnId == "peapleLeftBtn"){
        pealeMenuPersonId--;
        pealeMenuSwap()
    }
    if(targetBtnId == "peapleRightBtn"){
        pealeMenuPersonId++;
        pealeMenuSwap()
    }
}
function pealeMenuSwap(){
    document.querySelector('.peapleMenu_active').classList.remove("peapleMenu_active");
    if(pealeMenuPersonId == 5){
        pealeMenuPersonId = 1;
    }
    if(pealeMenuPersonId == 0){
        pealeMenuPersonId = 4;
    }
    peapleList[pealeMenuPersonId-1].classList.add('peapleMenu_active')
    document.querySelector('.peaple_avatar').src = `./images/peapleMenu/${pealeMenuPersonId}.png`
    console.log(pealeMenuPersonId);

    let name = document.querySelector('.peaple_name');
    let prof = document.querySelector('.peaple_prof');

    name.textContent = `${peaple_info[pealeMenuPersonId].name}`;
    prof.textContent = `${peaple_info[pealeMenuPersonId].prof}`;

}

//---------------